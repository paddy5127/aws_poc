package android.cranberry.aswpoc;

import android.util.Log;

/**
 * @Author: Pramod Jyotiram Waghmare
 * @Company: Cranberry Analytics Pvt. Ltd.
 * @Date: 18/8/21
 */
public class Utils {
    public static void showLog(String message){
        Log.d(Utils.class.getSimpleName(),message);
    }
}

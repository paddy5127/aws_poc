package android.cranberry.aswpoc

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.amplifyframework.api.ApiException
import com.amplifyframework.api.graphql.GraphQLResponse
import com.amplifyframework.api.graphql.model.ModelMutation
import com.amplifyframework.api.graphql.model.ModelQuery
import com.amplifyframework.core.Amplify
import com.amplifyframework.datastore.generated.model.Demo
import com.amplifyframework.datastore.generated.model.Todo
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.content_main.*
import kotlin.random.Random


class MainActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))


        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { _ ->
//            saveTodoViaAPI()
            saveTodo()
        }


        btnFetchAll.setOnClickListener {
//            fetchAllTodoViaAPI()
            fetchAllTodo()
        }


    }


    private fun saveTodo(){
        val todo = Todo.builder()
            .name("My first todo "+ Random(50).nextInt())
            .description("todo description "+Random(50).nextInt())
            .build()
        Amplify.DataStore.save(todo,
            {   Utils.showLog("Created a new todo via DS successfully ${it.item().id}")},
            {
                Utils.showLog("Created a new todo via DS failed ${it.localizedMessage}")
                it.printStackTrace()
            })
/*
        val demo = Demo.builder().name("Pramod "+Random(50).nextInt())
            .age(Random(10).nextInt())
            .build()

        Amplify.DataStore.save(demo,{
            Utils.showLog("Created a new Demo via DS successfully ${it.item().id}")
        },{
            Utils.showLog("Created a new todo via DS failed ${it.localizedMessage}")
        })*/

    }

    @SuppressLint("SetTextI18n")
    private fun fetchAllTodo(){
        Amplify.DataStore.query(Todo::class.java,
            {
                var textToRender = "FETCH ALL TODO\n\n"
                it.forEachRemaining { it ->
                    textToRender += "AMPLIFY FETCH DATA TODO ID: ${it.id}\n\n"
                }
                runOnUiThread {
                    txtPostInfo.text = textToRender
                }
            },
            {
                runOnUiThread {
                    txtPostInfo.text = "Error: ${it.localizedMessage}"
                }
            })

        /*Amplify.DataStore.query(Demo::class.java,
            {
                var textToRender = "FETCH ALL DEMO\n\n "

                it.forEachRemaining { it ->
                    textToRender += "AMPLIFY FETCH DATA DEMO ID: ${it.id}\n\n"
                }
                runOnUiThread {
                    txtPostInfo.text = txtPostInfo.text.toString() + "\n\n\n"+ textToRender
                }
            },
            {
                runOnUiThread {
                    txtPostInfo.text = "Error: ${it.localizedMessage}"
                }
            })*/
    }

    private fun fetchAllTodoViaAPI() {
        Utils.showLog("AMPLIFY FETCH DATA:")

        Amplify.API.query(
            ModelQuery.list(Todo::class.java),
            { response ->
                var textToRender = ""
                response.data.forEach { todo ->
                    Log.i("MyAmplifyApp", todo.name)
                    textToRender += "AMPLIFY FETCH DATA TODO ID: ${todo!!.id}\n\n"
                }
                runOnUiThread {
                    txtPostInfo.text = textToRender
                }
            },
            { Log.e("MyAmplifyApp", "Query failure", it)
                runOnUiThread {
                    txtPostInfo.text = it.localizedMessage
                }
            }
        )
        /*Amplify.DataStore.query(
            Post::class.java,
            { queryMatches: Iterator<Post?> ->
                var textToRender = ""
                if (queryMatches.hasNext()) {
                    textToRender = "AMPLIFY FETCH DATA successful query, found posts\n\n"
                    Utils.showLog("AMPLIFY FETCH DATA successful query, found posts::")
                } else {
                    textToRender = "AMPLIFY FETCH DATA successful query no posts\n\n"
                    Utils.showLog("AMPLIFY FETCH DATA successful query, no posts::")
                }
                queryMatches.forEachRemaining {
                    textToRender += "AMPLIFY FETCH DATA posts ID: ${it!!.id}\n\n"
                    Utils.showLog("AMPLIFY FETCH DATA posts ID: ${it!!.id}")
                }
                runOnUiThread {
                    txtPostInfo.text = textToRender
                }
            }
        ) { error: DataStoreException? ->
            error!!.printStackTrace()
            runOnUiThread {
                txtPostInfo.text = "AMPLIFY FETCH DATA failed query ::${error!!.message}"
            }
            Utils.showLog("AMPLIFY FETCH DATA failed query ::${error!!.message}")
        }*/
    }

    private fun saveTodoViaAPI() {
        val todo = Todo.builder()
            .name("My first todo")
            .description("todo description")
            .build()

        Amplify.API.mutate(
            ModelMutation.create(todo),
            { response: GraphQLResponse<Todo> ->
                Utils.showLog("Created a new todo successfully ${response.data.id}")
                Log.i(
                    "MyAmplifyApp",
                    "Added Todo with id: " + response.data.id
                )
            }
        ) { error: ApiException? ->
            Utils.showLog("Created a new todo failed ${error!!.message}")
            Log.e(
                "MyAmplifyApp",
                "Create failed",
                error
            )
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
package android.cranberry.aswpoc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.amazonaws.services.cognitoidentityprovider.model.UsernameAttributeType;
import com.amplifyframework.auth.AuthUserAttribute;
import com.amplifyframework.auth.AuthUserAttributeKey;
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin;
import com.amplifyframework.auth.options.AuthSignUpOptions;
import com.amplifyframework.core.Amplify;

import java.util.ArrayList;
import java.util.Random;

/**
 * @Author: Pramod Jyotiram Waghmare
 * @Company: Cranberry Analytics Pvt. Ltd.
 * @Date: 20/8/21
 */
public class Splashscreen extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        /*Amplify.Auth.signOut(
                () -> Log.i("AuthQuickstart", "Signed out successfully"),
                error -> Log.e("AuthQuickstart", error.toString())
        );*/

        checkIfUserLogin();

    }

    private void checkIfUserLogin() {
 /*       if(Amplify.Auth.getCurrentUser() == null) {
//            confirmUser();
//               register();
            loginUser();
        }else{
            Utils.showLog("AMPLIFY AUTH user not null:"+Amplify.Auth.getCurrentUser().getUserId());
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }*/
        if (Amplify.Auth.getCurrentUser() != null)
            Utils.showLog("AMPLIFY AUTH user not null:"+Amplify.Auth.getCurrentUser().getUserId());
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    private void loginWithWebUI() {
        Amplify.Auth.signInWithWebUI(
                this,
                result -> Log.i("AuthQuickStart", result.toString()),
                error -> Log.e("AuthQuickStart", error.toString())
        );
    }

    private void loginUser() {
        Amplify.Auth.signIn(
                "paddy11",
                "Password123",
                result -> {Log.i("AuthQuickstart", result.isSignInComplete() ? "Sign in succeeded" : "Sign in not complete");
                checkIfUserLogin();},
                error -> Log.e("AuthQuickstart", error.toString())
        );
    }

    private void confirmUser() {
        Amplify.Auth.confirmSignUp(
                "paddy11",
                "757780",
                result -> Log.i("AuthQuickstart", result.isSignUpComplete() ? "Confirm signUp succeeded" : "Confirm sign up not complete"),
                error -> Log.e("AuthQuickstart", error.toString())
        );
    }

    private void register(){
        Utils.showLog("AMPLIFY AUTH to register");
        ArrayList<AuthUserAttribute> authUserAttributes = new ArrayList<>();
        authUserAttributes.add(new AuthUserAttribute(AuthUserAttributeKey.email(),"ppatil5127@gmail.com"));
        authUserAttributes.add(new AuthUserAttribute(AuthUserAttributeKey.phoneNumber(),
                "+918208133613"));

        AuthSignUpOptions options = AuthSignUpOptions.builder()
                .userAttributes(authUserAttributes)
                .build();

        String username = "paddy"+ String.valueOf(Math.abs(new Random(2).nextInt())).substring(0,2);
        Utils.showLog("AMPLIFY AUTH USERNAME:"+username);

        Amplify.Auth.signUp(username, "Password123",options,
                result ->{ Log.i("AuthQuickStart", "Result: " + result.toString());
                    Utils.showLog("AMPLIFY AUTH register success: "+result.getUser().getUserId());},
                error ->{ Log.e("AuthQuickStart", "Sign up failed", error);
                    Utils.showLog("AMPLIFY AUTH register failed: "+error.getMessage());}
        );
    }
}
